require 'rails_helper'

RSpec.describe "static_pages/home.html.erb", :type => :view do
  it "should have_content 'Sample App'" do
  	render
  	expect(rendered).to have_content("Sample App")
  end
end
