require 'rails_helper'

RSpec.describe "UserPages" do
  subject {page}

  describe "Index action" do
    let(:user){FactoryGirl.create(:user)}
    let(:another_user){FactoryGirl.create(:user, email: 'some@mail.com')}
    let(:yet_another_user){FactoryGirl.create(:user, email: "yetmail@ma.com")}

    before(:each) do
      sign_in user
      visit users_path 
    end

    it {should have_title('All Users')}
    it {should have_content('All Users')}

    context 'pagination' do
      before(:all) { 31.times {FactoryGirl.create(:user)}}
      after(:all) {User.delete_all}

      it { should have_selector('div.pagination')}
      it "should display users" do
        User.paginate(page: 1).each do |user|
          expect(page).to have_selector('li', text: user.name)
        end
      end
    end

    context 'as an admin' do
      let(:admin){FactoryGirl.create(:admin)}
      before do
        sign_in admin
        visit users_path
      end

      it {should have_link('Delete', href: user_path(User.first))}
      it 'should be able to delete users' do
        expect do
          click_link('Delete', match: :first)
        end.to change(User, :count).by(-1)
      end
      it {should_not have_link('Delete', href: user_path(admin))}
    end
  end

  describe 'Edit' do
    let(:user){FactoryGirl.create(:user)}
    before do 
      sign_in user
      visit edit_user_path(user)
    end

    context 'page' do
      it { should have_content("Update your profile")}
      it { should have_title("Edit user")}
      it { should have_link('Change', href: 'http://gravatar.com/emails')}
    end

    context 'with valid info' do
      let(:new_name){'New Name'}
      let(:new_email){'new@example.com'}
      before do
        fill_in 'Email', with: new_email
        fill_in 'Name', with: new_name
        fill_in 'Password', with: user.password
        fill_in 'Confirm', with: user.password
        click_button 'Save changes'
      end

      it { should have_title(new_name)}
      it { should have_link('Sign out', href: signout_path)}
      it { should have_selector('div.alert.alert-success')}
      specify {expect(user.reload.name).to eq new_name}
      specify {expect(user.reload.email).to eq new_email}
    end

    context 'with invalid info' do
      before {click_button 'Save changes'}

      it { should have_content("Error")}
    end

    context 'forbidden attributes' do
      let(:params) do
        { user: { admin: true, password: user.password, password_confirmation: user.password}}
      end

      before do
        sign_in user
        patch user_path(user), params
      end

      specify { expect(user.reload).not_to be_admin }
    end
  end

  describe 'signup page' do
  	
    let(:submit){ "Create my account"}    

    before(:each) do
      ActionMailer::Base.deliveries.clear
  		visit signup_path
  	end

  	it {should have_content("Sign up")}
  	it {should have_title(full_title('Sign up'))}

    context  "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end

      context 'after submission' do
        before { click_button submit }

        it { should have_title('Sign up')}
        it { should have_content('error')}
      end
    end

    context "with valid information with account activation" do
      before do
        fill_in "Name", with: "Example User"
        fill_in "Email", with: "user@example.com"
        fill_in "Password", with: 'foobar'
        fill_in "Confirm", with: 'foobar'
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      it 'should change ActionMailer deliveries by 1' do
        expect { click_button submit }.to change(ActionMailer::Base.deliveries, :size).by(1)
      end

      context 'after saving the user' do
        before { click_button submit }        
        let(:user) { User.find_by(email: 'user@example.com')}

        # it { should have_title(user.name)}
        # it { should have_link('Sign out', href: signout_path)}
        it { should have_selector('div.alert.alert-success', text: 'Check your email for activation details')}
        
        it 'should not activate user' do
          expect(user.activated).to eq false
        end

        it 'should not log in with invalid activation token' do
                     
          get edit_account_activation_path('invalid token')          

          expect(response).to redirect_to(root_url)
          follow_redirect!
          expect(response.body).to include('Invalid activation link') 
        end        

        it 'should not log in with wrong email' do
          user.activation_token = User.new_remember_token       
          get edit_account_activation_path(user.activation_token, email: 'wrong')          

          expect(response).to redirect_to(root_url)
          follow_redirect!
          expect(response.body).to include('Invalid activation link') 
        end

        # it 'should log in with all valid' do
          
        #   get edit_account_activation_path(user.activation_token, email: user.email)          

        #   expect(response).to redirect_to(user)
        #   follow_redirect!
        #   expect(response.body).to include('Account has been successfully activated')
        #   expect(user.reload.activated).to eq true 
        # end

        context 'shoud not log in before activation' do
          before {
            visit signin_path
            fill_in "Email", with: "user@example.com"
            fill_in "Password", with: 'foobar'
            click_button 'Sign in'
          }

          it {should have_selector('div.alert.alert-info', text: "Check your email for account activation details")}
        end
      end      
    end    
  end

  context "profile(show) page" do
  	let(:user) { FactoryGirl.create(:user) }
    let!(:m1) { FactoryGirl.create(:micropost, user: user, content: 'Lorem') }
    let!(:m2) { FactoryGirl.create(:micropost, user: user, content: 'Ipsum') } 
  	before { visit user_path(user)}

  	it { should have_content(user.name)}
  	it { should have_title(user.name)}

    describe 'microposts' do
      it { should have_content(m1.content) }
      it { should have_content(m2.content) }
      it { should have_content(user.microposts.count) }
    end
  end
end
