require 'rails_helper'

RSpec.describe "AuthenticationPages", :type => :request do

  subject { page }

  describe 'Authorization' do

    describe 'as wrong user' do
      let(:user){FactoryGirl.create(:user)}
      let(:wrong_user){FactoryGirl.create(:user, email: 'wrong@example.com')}
      before { sign_in user, no_capybara: true }

      describe 'submitting GET request to the Users#edit action' do
        before { get edit_user_path(wrong_user)}

        specify { expect(response.body).not_to match(full_title('Edit user'))}
        specify { expect(response).to redirect_to (root_url)}
      end

      describe "submitting a PATCH request to the Users#update action" do
        before { patch user_path(wrong_user) }
        specify { expect(response).to redirect_to(root_url) }
      end
    end

    describe 'for non-signed in user' do
      let(:user){ FactoryGirl.create(:user)}
      let(:non_admin){FactoryGirl.create(:user)}      

      context 'when submitting DELETE request to Users#destroy action' do
        before do 
          sign_in non_admin
          delete user_path(user)
        end

        specify { expect(response).to redirect_to root_url}
      end

      context 'visiting index action should redirect to sigin' do
        before { visit users_path }
        it{should have_title('Sign in')}
      end

      context "when attempting to visit protected page friendly forwarding" do
        before(:each) do
          visit edit_user_path(user)
          #here comes redirect to signin page
          fill_in 'Email', with: user.email
          fill_in 'Password', with: user.password
          click_button 'Sign in'
        end

          it { should have_title('Edit user')}
      end

      describe 'in the UsersController' do
        describe "submitting to the update action" do
          before { patch user_path(user) }
          specify { expect(response).to redirect_to(signin_path) }
        end

        describe 'visiting the edit page' do
          before { visit edit_user_path(user)}

          it {should have_content('Sign in')}
        end        
      end
    end

    context 'in the Microposts controller' do
      context 'submitting to the create action' do
        before { post microposts_path }
        specify { expect(response).to redirect_to(signin_path) }
      end

      describe "submitting to the destroy action" do
        before { delete micropost_path(FactoryGirl.create(:micropost)) }
        specify { expect(response).to redirect_to(signin_path) }
      end
    end
  end

  describe 'signin' do
  	before { visit signin_path }

  	context 'with valid information' do
  		let(:user){ FactoryGirl.create(:user)}

  		before { sign_in user }

  		it { should have_title(user.name)}
      it { should have_link("Users", href: users_path)}
      it { should have_link('Settings', href: edit_user_path(user))}
  		it { should have_link("Profile", href: user_path(user))}
  		it { should have_link('Sign out', href: signout_path)}
  		it { should_not have_link("Signin", href: signin_path)}

      context 'followed by signout' do
        before { click_link 'Sign out'}
        it { should have_link('Sign in')}
      end
  	end

  	context 'with invalid information' do
  		before { click_button 'Sign in'}

  		it { should have_content('Sign in')}
  		it { should have_selector('div.alert.alert-danger')}

  		context 'after visiting another page' do
  			before { click_link 'Home'}

  			it { should_not have_selector('div.alert.alert-danger')}
  		end
  	end
  end
end
