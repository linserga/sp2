require 'rails_helper'

RSpec.describe Micropost, :type => :model do
  let(:user) { FactoryGirl.create(:user) }

  before do
  	@micropost = user.microposts.build(content: 'Lorem')
  end

  subject {@micropost}

  it { should respond_to(:content) }
  it { should respond_to(:user_id) }
  it { should respond_to(:user)}
  its(:user) { should eq user}

  it { should be_valid }

  context 'it should not be valid without user_id' do
  	before do
  		@micropost.user_id = nil
  	end

  	it { should_not be_valid }
  end

  context 'it should not be valid without content' do
  	before { @micropost.content = ''}

  	it { should_not be_valid }
  end

  context "it's content should be up to 140 characters" do
  	before { @micropost.content = "a" * 141 }

  	it { should_not be_valid }
  end
end
