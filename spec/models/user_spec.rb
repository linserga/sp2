require 'rails_helper'

RSpec.describe User, :type => :model do

	let!(:user){ User.new(name: 'Example User', 
		email: 'user@example.com', 
		password: 'foobar', 
		password_confirmation: "foobar")}

	subject { user }

	it { should respond_to(:name)}
	it { should respond_to(:email)}
	it { should respond_to(:password_digest)}
	it { should respond_to(:password)}
	it { should respond_to(:password_confirmation)}
	it { should respond_to(:authenticate)}
	it { should respond_to(:remember_token)}
	it { should respond_to(:admin)}
	it { should respond_to(:activated)}
	it { should respond_to(:microposts) }
	it { should respond_to(:feed) }
	it { should be_valid}

	context "with admin attribute set to true" do
		before do
		 user.save!
		 user.toggle!(:admin)
		end

		it {should be_admin}
	end

	context "when name is not present" do
		before { user.name = "  "}
		it { should_not be_valid }
	end

	context "when email is not present" do
		before { user.email = "  "}
		it { should_not be_valid }
	end

	context "when name is too long" do
		before { user.name = "a" * 51 }
		it { should_not be_valid }
	end

	context "when email is too long" do
		before { user.email = "a" * 256 }
		it { should_not be_valid }
	end

	context "when email format is invalid" do
		it "should be invalid" do
			invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
			
			invalid_addresses.each do |invalid_address|
				user.email = invalid_address
				expect(user).not_to be_valid
			end                           
		end
	end

	context "when email format is valid" do 
		it "should be valid" do
			valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
			
			valid_addresses.each do |valid_address|
				user.email = valid_address
				expect(user).to be_valid
			end                         
		end
	end

	context "when email address has been already taken" do
		before do
			duplicate_user = user.dup
			duplicate_user.email = user.email.upcase
			duplicate_user.save
		end

		it { should_not be_valid }
	end

	context "when password is not present" do
		before { user.password = user.password_confirmation = ""}
		it { should_not be_valid }
	end

	context "when password doesn't match confirmation" do
		before { user.password_confirmation = 'mismatch'}
		it { should_not be_valid }
	end

	context "when a password is too short" do
		before { user.password = user.password_confirmation = "aa"}
		it { should be_invalid }
	end

	describe "return value of authenticate method" do
		before { user.save }
		let(:found_user){ User.find_by(email: user.email)}

		context "with valid password" do
			it { should eq found_user.authenticate(user.password)}
		end

		context "with invalid password" do
			let(:user_for_invalid_password){ found_user.authenticate("invalid")}

			it { should_not eq user_for_invalid_password }
			specify { expect(user_for_invalid_password).to eq false }
		end
	end

	context 'remember token' do
		before { user.save }
		its(:remember_token) { should_not be_blank }
	end

	context 'authenticated? should return false for a user with nil digest' do
		specify {expect(user.authenticated?(:reset, '')).to eq false }
	end

	context 'microposts associations' do
		before { user.save }
		let!(:older_micropost) do
			FactoryGirl.create(:micropost, user: user, created_at: 1.day.ago)
		end

		let!(:newer_micropost) do
			FactoryGirl.create(:micropost, user: user, created_at: 1.hour.ago)
		end

		it 'should have the right micropost in the right order' do
			expect(user.microposts.to_a).to eq [newer_micropost, older_micropost]
		end

		it 'should destroy associated microposts' do
			microposts = user.microposts.to_a

			user.destroy
			expect(microposts).not_to be_empty
			microposts.each do |micropost|
				expect(Micropost.where(id: micropost.id)).to be_empty
			end
		end

		describe 'status' do
			let(:unfollowed_post) do
				FactoryGirl.create(:micropost, user: FactoryGirl.create(:user))
			end

			its(:feed) { should include(newer_micropost) }
			its(:feed) { should include(older_micropost) }
			its(:feed) { should_not include(unfollowed_post) }
		end
	end
end
