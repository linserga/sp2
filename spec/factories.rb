FactoryGirl.define do
	factory :user do
		sequence(:name) {|n| "Person #{n}"} 
		sequence(:email){|n| "person_#{n}@example.com"}
		password "foobar"
		password_confirmation "foobar"
		activated true
		activated_at Time.now.to_datetime
		reset_token User.new_remember_token		

		factory :admin do
			admin true
		end		
	end

	factory :micropost do
		content 'Lorem ipsum'
		user
	end
end