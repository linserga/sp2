class User < ActiveRecord::Base
	attr_accessor :activation_token, :reset_token
	before_create :create_remember_token 
	before_save { self.email.downcase! }
	before_create :create_account_activation
	has_secure_password 
	has_many :microposts, dependent: :destroy

	validates :name, presence: true, length: { maximum: 50 }
	validates :email, presence: true,
	                  length: { maximum: 255 },
	                  format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create},
	                  uniqueness: { case_sensitive: false}
	validates :password, length: { minimum: 3 }	                  

	def self.new_remember_token
		SecureRandom.urlsafe_base64
	end

	def self.encrypt token
		Digest::SHA1.hexdigest(token.to_s)
	end

	def authenticated? attribute, token
		digest = self.send("#{attribute}_digest")
		return false if digest.nil?
		digest === User.encrypt(token)
	end

	def create_password_reset
		self.reset_token = User.new_remember_token
		self.reset_digest = User.encrypt(self.reset_token)
	end

	def password_reset_expired?
		self.reset_sent_at < 1.minute.ago
	end

	def feed
		Micropost.where('user_id = ?', self.id)
	end

	private
		def create_remember_token
			self.remember_token = User.encrypt(User.new_remember_token)
		end

		def create_account_activation
			self.activation_token = User.new_remember_token
			self.activation_digest = User.encrypt(self.activation_token)
		end
end
