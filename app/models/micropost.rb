class Micropost < ActiveRecord::Base
	belongs_to :user
	default_scope { order(created_at: :desc) }

	validates :user_id, presence: true
	validates :content, presence: true, length: { maximum: 140 }
	validate :image_size
	mount_uploader :picture, PictureUploader

	private

		def image_size
			if picture.size > 5.megabytes
				errors.add(:picture, 'should be less than 5MB')
			end
		end
end
