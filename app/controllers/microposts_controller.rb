class MicropostsController < ApplicationController
  before_action :signed_in_user 
  before_action :correct_user, only: [:destroy]

  def create
  	@micropost = current_user.microposts.build(micropost_params)

  	if @micropost.save
  		flash[:success] = 'Micropost created'
  		redirect_to root_url
  	else
      @feed_items = current_user.feed.paginate(page: params[:page])
  		render 'static_pages/home'
  	end
  end

  def destroy
    @micropost.destroy
    flash[:success] = 'Micropost was destroyed'
    redirect_to root_url
  end

  private 
  	def micropost_params
  		params.require(:micropost).permit(:content, :picture)
  	end

    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url, danger: "You are not the owner of the micropost" if @micropost.nil?
    end

    # Alternative way to write this method. Here we use find becasue it will raise an error if 
    # @micropost is nil
    # def correct_user
    #   @micropost = current_user.microposts.find(params[:id])
    # rescue
    #   redirect_to root_url
    # end
end
