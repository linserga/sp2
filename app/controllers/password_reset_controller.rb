class PasswordResetController < ApplicationController
  
  before_action :get_user, only: [:edit, :update]
  before_action :verify_user, only: [:edit, :update]
  before_action :expiration_check, only: [:edit, :update]

  def new
  end

  def create
  	@user = User.find_by email: params[:password_reset][:email]

  	if @user 
  		flash[:info] = 'Check your email for password reset details'
  		@user.create_password_reset
  		@user.update_columns(reset_digest: @user.reset_digest, reset_sent_at: Time.now.to_datetime)
  		UserMailer.password_reset(@user).deliver_now
  		redirect_to root_url
  	else
  		flash.now[:danger] = 'There is no such email it the database'
  		render :new
  	end
  end

  def edit
  end

  def update
  	if password_blank?
  		flash.now[:info] = "Password can't be blank"
  		render :edit
  	elsif @user.update_attributes(user_params)
  		flash[:success] = 'Password has been updated'
  		sign_in @user
  		redirect_to @user
  	else
  		render :edit
  	end
  end

  private
  	def verify_user
  		unless @user && @user.activated? && @user.authenticated?(:reset, params[:id])
  			redirect_to root_url
  		end
  	end

  	def get_user
  		@user = User.find_by email: params[:email]
  	end

  	def user_params
  		params.require(:user).permit(:password, :password_confirmation)
  	end

  	def password_blank?
  		params[:user][:password].blank?
  	end

  	def expiration_check
  		if @user.password_reset_expired?
  			flash[:danger] = 'Link has expired'
  			redirect_to new_password_reset_url
  		end
  	end
end
