class AccountActivationController < ApplicationController
  def edit
  	@user = User.find_by email: params[:email]

  	if @user && !@user.activated? && @user.authenticated?(:activation, params[:id])
  		@user.update_columns(activated: true, activated_at: Time.now.to_datetime)
  		sign_in @user
  		flash[:success] = 'Account has been successfully activated'
  		redirect_to @user
  	else
  		flash[:danger] = 'Invalid activation link'
  		redirect_to root_url
  	end
  end
end
