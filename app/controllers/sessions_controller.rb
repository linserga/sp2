class SessionsController < ApplicationController
	def new
	end

	def create
		@user = User.find_by(email: params[:session][:email])
		if @user && @user.authenticate(params[:session][:password])	
			if @user.activated?
				sign_in @user		
				redirect_back_or @user
			else
				flash.now[:info] = 'Check your email for account activation details'
				render :new
			end
		else
			flash.now[:danger] = 'Invalid email/password combination'
			render :new
		end
	end

	def destroy
		sign_out
		redirect_to root_url
	end
end
