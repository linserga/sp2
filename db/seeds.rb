# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!( name: 'Serge',
							email: 'example@railstutorial.org',
							password: 'foobar',
							password_confirmation: 'foobar',
							admin: true,
							activated: true,
							activated_at: Time.now.to_datetime
						)

99.times do |n|
	User.create!(
			name: Faker::Name.name,
			email: "example-#{n+1}@railstutorial.org",
			password: 'foobar',
			password_confirmation: 'foobar',
			activated: true,
			activated_at: Time.now.to_datetime
		)
end

users = User.all.limit(6)

50.times do
	users.each do |user|
		user.microposts.create!(content: Faker::Lorem.sentence(5))
	end
end

puts "***DONE***"